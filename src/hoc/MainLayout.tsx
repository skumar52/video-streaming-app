import React, { useState } from "react";
import Sidebar from "@/components/Sidebar";
import SearchBar from "@/components/SearchBar";
import Modal from "@/components/Modal";

const MainLayout = (Component: React.FC<any>) => {
  return (props: any) => {
    const [search, setSearch] = useState("");

    const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
      const { value } = event.target;

      setSearch(value);
    };

    return (
      <div className="flex h-screen gap-10">
        <Sidebar />
        <div className="w-full h-full pt-10 ml-24 ">
          <SearchBar handleChange={handleChange} />
          <Modal data={search} />
          <Component {...props} />
        </div>
      </div>
    );
  };
};

export default MainLayout;
