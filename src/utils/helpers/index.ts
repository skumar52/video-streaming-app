import axios from "axios";

export let cancelToken: undefined | Function;

const api = (method: string, url: string, params?: any) => {
  const domain = "https://youtube-search-and-download.p.rapidapi.com";

  const options = {
    method,
    url: domain + url,
    headers: {
      Accept: "application/json",
      "Content-Type": "application/json",
      "X-RapidAPI-Key": "1aece9e281mshc2acfa319c683a4p1bbc74jsn80cd5bd67ca3",
      "X-RapidAPI-Host": "youtube-search-and-download.p.rapidapi.com",
    },
    params,
    cancelToken: new axios.CancelToken((c) => (cancelToken = c)),
  };

  return axios(options);
};

export default api;
