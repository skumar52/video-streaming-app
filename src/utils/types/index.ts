export type Url = {
  height: number;
  url: string;
  width: number;
};

export type VideoData = {
  video: {
    channelId: string;
    channelName: string;
    lengthText: string;
    publishedTimeText: string;
    thumbnails: Url[];
    title: string;
    videoId: string;
    viewCountText: string;
  };
};

export type ContentType = {
  authorId: string;
  authorName: string;
  authorThumbnails: Url[];
  likes: string;
  publishedTimeText: string;
  replyCount: string;
  replyNext: string;
  text: string;
};

export type CurrentVideo = {
  captions: null;
  streamingData: {
    adaptiveFormats: any[];
    expiresInSecond: string;
    formats: any[];
  };
  videoDetails: any;
};
