import { MdOndemandVideo, MdMonitor, MdMovieCreation } from "react-icons/md";
import { BsBookmarksFill } from "react-icons/bs";
import { BiBookContent } from "react-icons/bi";

export const icons = [
  {
    icon: BsBookmarksFill,
  },
  {
    icon: MdMonitor,
  },

  {
    icon: MdOndemandVideo,
  },
  {
    icon: BiBookContent,
  },
];

export const MovieIcon = MdMovieCreation;
