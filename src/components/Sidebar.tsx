import React from "react";
import { useRouter } from "next/router";
import { icons, MovieIcon } from "@/utils/constant";

export default function Sidebar() {
  const router = useRouter();

  return (
    <div
      className="flex justify-center  h-[95%] w-10 rounded-md  w-10 p-8 bg-slate-700 ml-4 py-6 mt-6 "
      style={{ position: "fixed" }}
    >
      <div className="flex flex-col gap-10  ">
        <div className="cursor-pointer">
          <MovieIcon size={40} color="red" onClick={() => router.push("/")} />
        </div>
        <div className="flex  flex-col items-center gap-8">
          {icons.map((item) => (
            <div className="cursor-pointer">
              {<item.icon size={25} color="#64748B" />}
            </div>
          ))}
        </div>
      </div>
    </div>
  );
}
