import React from "react";

import { icons } from "@/utils/constant";
import { useRouter } from "next/router";
import Card from "@/components/Card";
import { VideoData } from "@/utils/types";

type AppProps = {
  data: any;
  isBlur: boolean;
};

export default function BookMarked({ data, isBlur }: AppProps) {
  const BookMark = icons[0].icon;
  const router = useRouter();

  return (
    <div className={`flex flex-col gap-4 text-white  ${isBlur && "blur-lg"}`}>
      <div>
        <h1 className="text-3xl">Trending</h1>
      </div>

      <div className="grid  md:grid-cols-4 md:gap-4 grid-cols-1 sm:grid-cols-2 ">
        {data.map((item: VideoData) => (
          <Card item={item} />
        ))}
      </div>
    </div>
  );
}
