import { Dispatch, SetStateAction } from "react";
import axios from "axios";
import api from "../helpers";
import { useEffect, useState } from "react";
import { VideoData } from "../types";
import { cancelToken } from "../helpers";

export default function useVideoSearch(
  query: string,
  next: string,
  setNext: Dispatch<SetStateAction<string>>,
  pageNumber: number,
  setIsOpen: Dispatch<SetStateAction<boolean>>
) {
  const [loading, setLoading] = useState(true);
  const [error, setError] = useState(false);
  const [videos, setVideos] = useState([] as VideoData[]);
  const [hasMore, setHasMore] = useState(false);

  useEffect(() => {
    setVideos([]);
    setError;
    if (!query) return setIsOpen(false);

    api("get", "/search", { query, next })
      .then((res) => {
        setVideos((prevVideos: VideoData[]) => {
          return [...prevVideos, ...res.data.contents];
        });
        setIsOpen(true);
        setNext(res.data.next);

        setHasMore(res.data.contents.length > 0);
        setLoading(false);
      })
      .catch((e) => {
        if (axios.isCancel(e)) return;
        setError(true);
      });

    // axios({
    //   method: "GET",
    //   url: "https://youtube-search-and-download.p.rapidapi.com/search",
    //   headers: {
    //     Accept: "application/json",
    //     "Content-Type": "application/json",
    //     "X-RapidAPI-Key": "0b0fbf6010mshc270f09affbdc11p1ae33cjsn6610d2b8334f",
    //     "X-RapidAPI-Host": "youtube-search-and-download.p.rapidapi.com",
    //   },
    //   params: { query, next: next },
    //   cancelToken: new axios.CancelToken((c) => (cancel = c)),
    // })
    //   .then((res) => {
    //     setVideos((prevVideos: VideoData[]) => {
    //       return [...prevVideos, ...res.data.contents];
    //     });
    //     setIsOpen(true);
    //     setNext(res.data.next);

    //     setHasMore(res.data.contents.length > 0);
    //     setLoading(false);
    //   })
    //   .catch((e) => {
    //     if (axios.isCancel(e)) return;
    //     setError(true);
    //   });
    return () => cancelToken && cancelToken();
  }, [query, pageNumber]);

  console.log("use", next);
  return { loading, error, videos, hasMore };
}
