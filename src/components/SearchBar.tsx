import React from "react";
import { BsSearch } from "react-icons/bs";

type AppProps = {
  handleChange: React.ChangeEventHandler<HTMLInputElement>;
};

export default function SearchBar({ handleChange }: AppProps) {
  return (
    <div className=" w-60 flex ">
      <div style={{ position: "absolute" }}>
        <input
          className="bg-transparent p-2 pl-[2.4rem] search-Input"
          placeholder="Search for movies or TV series"
          onChange={handleChange}
        />
      </div>
      <div className="d-flex p-2">
        <BsSearch color="white" size={25} style={{ position: "relative" }} />
      </div>
    </div>
  );
}
