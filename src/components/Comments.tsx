// import Avatar from "react-avatar";

import React from "react";
import { ContentType } from "@/utils/types";

type AppProps = {
  item: ContentType;
};

const Comments = ({ item }: AppProps) => {
  return (
    <div className="flex gap-6 ">
      <div style={{ maxWidth: "60px", maxHeight: "60px" }}>
        <img
          className="rounded-full"
          src={`${item.authorThumbnails[item.authorThumbnails.length - 1].url}`}
          alt="Rounded avatar"
        ></img>
      </div>
      <div>
        <p>{item.authorName}</p>
        <div>{item.text}</div>
      </div>
    </div>
  );
};

export default Comments;
