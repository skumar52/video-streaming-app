import React, { useEffect, useState } from "react";

import { useRouter } from "next/router";
import ReactPlayer from "react-player";
import isEmpty from "lodash/isEmpty";

import api from "@/utils/helpers";
import MainLayout from "@/hoc/MainLayout";
import Card from "@/components/Card";
import Comments from "@/components/Comments";
import { VideoData, ContentType, CurrentVideo } from "@/utils/types";

type AppProps = {
  currentVideo: CurrentVideo;
  relatedVideo: any;
  comments: any;
};

const Player = ({
  currentVideo,
  relatedVideo,
  comments,
}: AppProps): JSX.Element => {
  const router = useRouter();
  const slug = router.query.slug;

  const { streamingData, videoDetails } = currentVideo;
  const { contents } = relatedVideo;
  const { comments: comment } = comments;

  const [domLoaded, setDomLoaded] = useState(false);

  useEffect(() => {
    if (window !== undefined) {
      setDomLoaded(true);
    }
  }, [slug]);

  return domLoaded ? (
    <div className="ml-24  flex p-4 gap-6  ">
      <div className=" flex flex-col ">
        <div className="mb-10">
          <ReactPlayer
            playing={true}
            controls
            url={
              !isEmpty(streamingData.formats) &&
              streamingData.formats[streamingData.formats.length - 1].url
            }
            width="100%"
            height="100%"
          />
        </div>
        <div className="flex gap-20 flex-col">
          <div>
            <p className="text-white text-2xl">Title: {videoDetails?.title} </p>
            <p className="text-white text-1xl  break-words">
              Description: {videoDetails?.shortDescription}
            </p>
          </div>
        </div>
        <div className="text-white flex flex-col gap-6 mt-4">
          {comment.map((item: ContentType) => (
            <Comments item={item} />
          ))}
        </div>
      </div>

      <div className="p-10 pt-8 text-white ">
        {contents.map((item: VideoData) => (
          <Card item={item} height="300px" width="400px" />
        ))}
      </div>
    </div>
  ) : (
    <></>
  );
};

export async function getServerSideProps(context: any) {
  const { query } = context;

  const getData = await api("get", "/video", {
    id: query.slug,
  });

  const getRelatedVideo = await api("get", "/video/related", {
    id: query.slug,
  });

  const getVideoComments = await api("get", "/video/comments", {
    id: query.slug,
  });

  const currentVideo = await getData.data;
  const relatedVideo = await getRelatedVideo.data;
  const comments = await getVideoComments.data;

  return {
    props: { currentVideo, relatedVideo, comments }, // will be passed to the page component as props
  };
}

export default MainLayout(Player);
