import api from "@/utils/helpers";
import React, { useState, useRef, useCallback } from "react";
import Card from "./Card";
import isEmpty from "lodash/isEmpty";
import { AiOutlineClose } from "react-icons/ai";
import useVideoSearch from "@/utils/customHooks/useVideoSearch";
import Spinner from "./Spinner";

type AppProps = {
  data: string;
  // setBlur: React.Dispatch<React.SetStateAction<boolean>>;
};

const Modal = ({ data }: AppProps) => {
  const [isOpen, setIsOpen] = useState(false);
  const [next, setNext] = useState("");

  const [pageNumber, setPageNumber] = useState(1);

  const handleCloseModal = () => {
    setIsOpen(false);
  };

  const { videos, hasMore, loading, error } = useVideoSearch(
    data,
    next,
    setNext,
    pageNumber,
    setIsOpen
  );

  const observer: React.MutableRefObject<any> = useRef();
  const lastBookElementRef = useCallback(
    (node: React.ReactNode) => {
      if (loading) return;

      if (observer.current) observer.current.disconnect();

      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting && hasMore) {
          setPageNumber((prev) => prev + 1);
          console.log(node);
        }
      });
      if (node) observer.current.observe(node);
    },
    [loading, hasMore]
  );

  return (
    isOpen && (
      <div
        className=" rounded-lg  items bg-slate-700 flex p-26 overflow-y-scroll  scroll-hidden"
        style={{
          zIndex: "9999999",
          width: "60vw",
          height: "70vh",
          margin: "auto",
        }}
      >
        <div className=" close-icon" onClick={handleCloseModal}>
          <AiOutlineClose size={28} color="white" />
        </div>

        <div className="flex flex-col justify-between text-white w-full scroll-hidden ">
          <p className="text-center mt-6">Search Results</p>

          <div className="flex flex-col items-center gap-10  ">
            <div className="flex flex-wrap gap-10 m-auto pb-16 justify-center">
              {!isEmpty(videos) &&
                videos.map((item, index) => {
                  if (videos.length === index + 1) {
                    return <Card item={item} ref={lastBookElementRef} />;
                  } else {
                    return <Card item={item} />;
                  }
                })}
            </div>
            <Spinner />
          </div>
        </div>
      </div>
    )
  );
};

export default Modal;
