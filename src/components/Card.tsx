import React, { forwardRef } from "react";
import Image from "next/image";
import { useRouter } from "next/router";
import { VideoData } from "@/utils/types";

type AppProps = {
  item: VideoData;
  height?: string;
  width?: string;
};

const Card = forwardRef((props: AppProps, ref: any) => {
  const router = useRouter();

  const { item, height = "250px", width = "300px" } = props;
  const { video } = item;

  return (
    <>
      <div
        className="flex flex-col  cursor-pointer my-5 "
        onClick={() => router.push(`/player/${video.videoId}`)}
        style={{ height, width }}
        ref={ref}
      >
        <div
          className="bg-auto bg-no-repeat bg-center"
          style={{
            height: "100%",
            width: "100%",
            position: "relative",
          }}
        >
          <Image
            className="rounded-lg"
            src={video?.thumbnails[video.thumbnails.length - 1]?.url}
            fill={true}
            alt="ads"
            objectFit="center"
          />
        </div>

        <div className="flex gap-6 flex-wrap ">
          <span className="wrap">{video?.title}</span>
        </div>
      </div>
    </>
  );
});

export default Card;
