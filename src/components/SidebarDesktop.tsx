import React from "react";

import { icons, MovieIcon } from "@/utils/constant";
import Avatar from "react-avatar";

export default function SidebarDesktop() {
  return (
    <div
      className="h-full hidden md:block "
      style={{
        position: "fixed",
        zIndex: "100",
      }}
    >
      <div className="flex col  md:row justify-center  h-[95%] w-10 rounded-md  w-10 p-8 bg-slate-700 ml-4 py-6 mt-6  ">
        <div className="flex flex-col justify-between  ">
          <div className="flex-none">
            <MovieIcon size={40} color="red" />
          </div>
          <div className="flex  h-80 mb-8 justify-center align-center">
            <div className="flex  flex-col items-center gap-8">
              {icons.map((item) => (
                <div>{<item.icon size={25} color="#64748B" />}</div>
              ))}
            </div>
          </div>
          <div className="flex-2">
            <Avatar name="zyz" round size="35px" />
          </div>
        </div>
      </div>
    </div>
  );
}
