import React from "react";
import { icons } from "@/utils/constant";

type AppProps = {
  url: string;
  height: string;
  width: string;
  relaseYear: number;
  type: string;
  age: string;
  title: string;
};

export default function Trending({
  url,
  height,
  width,
  relaseYear,
  type,
  age,
  title,
}: AppProps) {
  const BookMark = icons[0].icon;
  return (
    <div className="flex flex-col justify-center ">
      <div
        className="bg-auto bg-no-repeat bg-center rounded-lg"
        style={{
          backgroundImage: `url(${url})`,
          height,
          width,
          position: "relative",
        }}
      >
        <div
          className="p-1 rounded-full bg-slate-700 mt-4 mr-2 "
          style={{
            position: "absolute",
            right: "0",
          }}
        >
          <BookMark />
        </div>
      </div>
      <div>
        <div className="flex gap-6 ">
          <span>{relaseYear}</span>
          <span>{type}</span>
          <span>{age}</span>
        </div>
        <div>{title}</div>
      </div>
    </div>
  );
}
