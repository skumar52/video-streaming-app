import React, { useState, useCallback } from "react";
import BookMarked from "@/layouts/BookMarked";
import api from "@/utils/helpers";
import MainLayout from "@/hoc/MainLayout";

const Index = ({ data }: any) => {
  return (
    <>
      <BookMarked data={data} />
    </>
  );
};

export async function getServerSideProps() {
  const getData = await api("get", "/trending", {
    gl: "IN",
    type: "n",
    hl: "en",
  });

  const data = await getData.data.contents;

  return {
    props: { data }, // will be passed to the page component as props
  };
}

export default MainLayout(Index);
