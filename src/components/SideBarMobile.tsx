import React from "react";

import { icons, MovieIcon } from "@/utils/constant";
import Avatar from "react-avatar";

export default function SidebarMobile() {
  return (
    <div className=" md:hidden mb-2">
      <div className="rounded-md p-2   bg-slate-700     ">
        <div className="flex  justify-between items-center ">
          <div className="flex-none">
            <MovieIcon size={40} color="red" />
          </div>
          <div className="flex   justify-center items-center">
            <div className="flex   items-center gap-8">
              {icons.map((item) => (
                <div>{<item.icon size={25} color="#64748B" />}</div>
              ))}
            </div>
          </div>
          <div className="flex-2">
            <Avatar name="zyz" round size="35px" />
          </div>
        </div>
      </div>
    </div>
  );
}
